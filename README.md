# Piechur Project

The Piechur Project is a university project focused on delivering a website system for local small agritourism businesses. It aims to help these businesses build complete offers for potential clients, including accommodations, food, and even optional trips. 

Every client can visit the site, browse through the available offers, and sign up for the ones that interest them. Additionally, registered users have the opportunity to become offer deliverers. The system allows these users to create offers from scratch and publish them. Once published, users can manage their offers and maintain a list of potential customers. 

The project is built using .NET Core 2.1 as the backend and Angular 6 as the frontend.

## Installation and Testing

To test and install this application, you will need:

- .NET Core 3.0 SDK with ASP.NET Core
- An IDE of your choice that supports .NET

The application can be run using basic dotnet commands (run, build, watch). Before that, the 'dotnet restore' command should be executed to restore the project's dependencies.. It will also run the client app automatically.
